Une fois le projet cloné allez dans le dossier api et tapez : composer install et dans le dossier kindred_front npm install 
Allez dans le .env dans le dossier api et modifiez vos informations de base de données 

//Pour créer la BDD
Tapez php bin/console doctrine:database:create

//Pour générer les entités en BDD
php bin/console schema:update --force

//Pour créer des données en BDD
php bin/console doctrine:fixtures:load


Pour faire fonctionner la partie authentification de l'application allez dans le dossier api/config et créez un dossier jwt 

//Ouvrez ensuite un terminal openssl et retournez à la racine de votre dossier api et tapez d'abord la commande :
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096

Un mot de passe vous sera demandé allez dans le fichier .env et récupérez le JWT_PASSPHRASE

collez le JWT_PASSPHRASE puis tapez entrée

//Ensuite rentrez cette commande puis entrée et copier coller le JWT_PASSPHRASE après puis entrée

openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
