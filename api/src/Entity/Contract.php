<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ContractRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;


/**
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"contract_read"}}
 *          },
 *          "post"
 *      },
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"contract_details_read"}}
 *          },
 *          "put",
 *          "patch",
 *          "delete"
 *      })
 * @ApiFilter(SearchFilter::class, properties={"parent", "child"})
 * @ORM\Entity(repositoryClass=ContractRepository::class)
 */
class Contract
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"contract_read", "contract_details_read","user_read", "user_details_read", "rule_read", "rule_details_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({"contract_read", "contract_details_read",  "user_details_read", "rule_read", "rule_details_read"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="contracts")
     * @Groups({"contract_read", "contract_details_read"})
     */
    private $parent;

    /**
     * @ORM\OneToOne(targetEntity=Users::class, cascade={"persist", "remove"})
     * @Groups({"contract_read", "contract_details_read"})
     */
    private $child;

    /**
     * @ORM\OneToMany(targetEntity=Rules::class, mappedBy="contract", cascade={"persist", "remove"})
     */
    private $rules;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"contract_read", "contract_details_read"})
     */
    private $isSigned;

    public function __construct()
    {
        $this->rules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getParent(): UserInterface
    {
        return $this->parent;
    }

    public function setParent(UserInterface $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChild(): ?Users
    {
        return $this->child;
    }

    public function setChild(?Users $child): self
    {
        $this->child = $child;

        return $this;
    }

    /**
     * @return Collection|Rules[]
     */
    public function getRules(): Collection
    {
        return $this->rules;
    }

    public function addRule(Rules $rule): self
    {
        if (!$this->rules->contains($rule)) {
            $this->rules[] = $rule;
            $rule->setContract($this);
        }

        return $this;
    }

    public function removeRule(Rules $rule): self
    {
        if ($this->rules->removeElement($rule)) {
            // set the owning side to null (unless already changed)
            if ($rule->getContract() === $this) {
                $rule->setContract(null);
            }
        }

        return $this;
    }

    public function getIsSigned(): ?bool
    {
        return $this->isSigned;
    }

    public function setIsSigned(bool $isSigned): self
    {
        $this->isSigned = $isSigned;

        return $this;
    }
}
