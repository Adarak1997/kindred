<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\WeekRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"week_read"}}
 *          },
 *          "post"
 *      },
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"week_details_read"}}
 *          },
 *          "put",
 *          "patch",
 *          "delete"
 *      }
 * )
 * @ApiFilter(SearchFilter::class, properties={"startWeek", "endWeek", "child"})
 * @ApiFilter(DateFilter::class, properties={"startWeek", "endWeek"})
 * @ApiFilter(BooleanFilter::class, properties={"is_activated"})
 * @ORM\Entity(repositoryClass=WeekRepository::class)
 */
class Week
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"week_read", "week_details_read", "task_read", "task_details_read"})
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity=Tasks::class, mappedBy="week")
     * @Groups({"week_read", "week_details_read"})
     */
    private $tasks;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"week_read", "week_details_read", "task_read", "task_details_read"})
     */
    private $is_activated;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"week_read", "week_details_read", "task_read", "task_details_read"})
     */
    private $startWeek;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"week_read", "week_details_read", "task_read", "task_details_read"})
     */
    private $endWeek;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"week_read", "week_details_read","task_read", "task_details_read"})
     */
    private $totalPoint;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="weeks")
     * @Groups({"week_read", "week_details_read"})
     */
    private $child;


    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }



    /**
     * @return Collection|Tasks[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Tasks $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setWeek($this);
        }

        return $this;
    }

    public function removeTask(Tasks $task): self
    {
        if ($this->tasks->removeElement($task)) {
            // set the owning side to null (unless already changed)
            if ($task->getWeek() === $this) {
                $task->setWeek(null);
            }
        }

        return $this;
    }


    public function getIsActivated(): ?bool
    {
        return $this->is_activated;
    }

    public function setIsActivated(bool $is_activated): self
    {
        $this->is_activated = $is_activated;

        return $this;
    }

    public function getStartWeek(): ?\DateTimeInterface
    {
        return $this->startWeek;
    }

    public function setStartWeek(\DateTimeInterface $startWeek): self
    {
        $this->startWeek = $startWeek;

        return $this;
    }

    public function getEndWeek(): ?\DateTimeInterface
    {
        return $this->endWeek;
    }

    public function setEndWeek(\DateTimeInterface $endWeek): self
    {
        $this->endWeek = $endWeek;

        return $this;
    }

    public function getTotalPoint(): ?int
    {
        return $this->totalPoint;
    }

    public function setTotalPoint(int $totalPoint): self
    {
        $this->totalPoint = $totalPoint;

        return $this;
    }

    public function getChild(): ?Users
    {
        return $this->child;
    }

    public function setChild(?Users $child): self
    {
        $this->child = $child;

        return $this;
    }

}
