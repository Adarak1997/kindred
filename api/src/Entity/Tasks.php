<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TasksRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"task_read"}}
 *          },
 *          "post"
 *      },
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"task_details_read"}}
 *          },
 *          "put",
 *          "patch",
 *          "delete"
 *      }
 * )
 * @ApiFilter(SearchFilter::class, properties={"libelle", "week", "users", "isActivated"})
 * @ORM\Entity(repositoryClass=TasksRepository::class)
 */
class Tasks
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"task_read", "task_details_read", "week_read", "week_details_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"task_read", "task_details_read", "week_read", "week_details_read"})
     */
    private $libelle;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"task_read", "task_details_read"})
     */
    private $points;


    /**
     * @ORM\ManyToOne(targetEntity=Status::class, inversedBy="tasks")
     * @Groups({"task_read", "task_details_read"})
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="tasks")
     * @Groups({"task_read", "task_details_read", "week_read", "week_details_read"})
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity=Week::class, inversedBy="tasks")
     * @Groups({"task_read", "task_details_read"})
     */
    private $week;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"task_read", "task_details_read", "week_read", "week_details_read"})
     */
    private $isValidated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }



    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getWeek(): ?Week
    {
        return $this->week;
    }

    public function setWeek(?Week $week): self
    {
        $this->week = $week;

        return $this;
    }

    public function getIsValidated(): ?bool
    {
        return $this->isValidated;
    }

    public function setIsValidated(bool $isValidated): self
    {
        $this->isValidated = $isValidated;

        return $this;
    }
}
