<?php

namespace App\DataFixtures;

use App\Entity\Role;
use App\Entity\Users;
use App\Entity\Contract;
use App\Entity\Rules;
use App\Entity\Status;
use App\Entity\Tasks;
use App\Entity\Week;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\Date;

class UsersFixtures extends Fixture
{
    private $encoder;


    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }



    public function load(ObjectManager $manager)
    {

        //Create 3 status
        $status_en_cours = new Status();
        $status_en_cours->setLibelle("en cours");
        $manager->persist($status_en_cours);

        $status_valide = new Status();
        $status_valide->setLibelle("validée");
        $manager->persist($status_valide);

        $status_echoue = new Status();
        $status_echoue->setLibelle("échoué");
        $manager->persist($status_echoue);

        $fake = Factory::create();

        //Create parent role
        $role_parent = new Role();
        $role_parent->setLibelle('parent');
        $manager->persist($role_parent);

        //Create child role
        $role_child= new Role();
        $role_child->setLibelle('enfant');
        $manager->persist($role_child);

            //Create parent user
            $parent = new Users();
            $passHash = $this->encoder->hashPassword($parent, 'password');

            $parent->setEmail($fake->email)
                ->setFirstname($fake->firstName('female'))
                ->setName($fake->lastName)
                ->setAge(40)
                ->setRole($role_parent)
                ->setPassword($passHash);

            $manager->persist($parent);

            //Create 2 child users
            $child = new Users();
            $passHash = $this->encoder->hashPassword($child, 'password');

            $child->setEmail($fake->email)
                ->setFirstname($fake->firstName('male'))
                ->setName($fake->lastName)
                ->setAge(12)
                ->setRole($role_child)
                ->setParent($parent)
                ->setPassword($passHash);

            $manager->persist($child);

        $child2 = new Users();
        $passHash = $this->encoder->hashPassword($child, 'password');

        $child2->setEmail($fake->email)
            ->setFirstname($fake->firstName('male'))
            ->setName($fake->lastName)
            ->setAge(12)
            ->setRole($role_child)
            ->setParent($parent)
            ->setPassword($passHash);

        $manager->persist($child2);

        //Create contract
        $contract = new Contract();
        $contract->setDescription("contrat avec mon enfant");
        $contract->setParent($parent);
        $contract->setChild($child);
        $contract->setIsSigned(false);
        $manager->persist($contract);

        //Create 2 rules
        $rule1 = new Rules();
        $rule1->setLibelle("respect des consignes");
        $rule1->setContract($contract);
        $manager->persist($rule1);

        $rule2 = new Rules();
        $rule2->setLibelle("respect au parent");
        $rule2->setContract($contract);
        $manager->persist($rule2);

        $week1 = new Week();
        $week1->setChild($child);
        $week1->setIsActivated(true);
        $week1->setTotalPoint(0);

        /**
         * @param DateTime $date
         */
        $date = new DateTime('08-05-2021');
        $date2 = new DateTime('15-05-2021');
        $week1->setStartWeek($date);
        $week1->setEndWeek($date2);
        $manager->persist($week1);

        $task1 = new Tasks();
        $task1->setLibelle("promener le chien");
        $task1->setUsers($child);
        $task1->setIsValidated(0);
        $task1->setPoints(5);
        $task1->setStatus($status_en_cours);
        $task1->setWeek($week1);
        $manager->persist($task1);

        $task2 = new Tasks();
        $task2->setLibelle("mettre le couvert");
        $task2->setUsers($child);
        $task2->setIsValidated(0);
        $task2->setPoints(5);
        $task2->setStatus($status_en_cours);
        $task2->setWeek($week1);
        $manager->persist($task2);

        $manager->flush();
    }
}
