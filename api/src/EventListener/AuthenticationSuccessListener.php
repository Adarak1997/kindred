<?php


namespace App\EventListener;


use App\Entity\Users;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

class AuthenticationSuccessListener
{

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();



        if ($user instanceof Users)

            if ($user->getParent() != null)  {
                $data['data'] = array(
                    'id' => $user->getId(),
                    'email' => $user->getEmail(),
                    'age' => $user->getAge(),
                    'firstname' => $user->getFirstname(),
                    'name' => $user->getName(),
                    'role_id' => $user->getRole()->getId(),
                    'role_libelle' => $user->getRole()->getLibelle(),
                    'parent_id' => $user->getParent()->getId()
                );
            }else {
                $data['data'] = array(
                    'id' => $user->getId(),
                    'email' => $user->getEmail(),
                    'age' => $user->getAge(),
                    'firstname' => $user->getFirstname(),
                    'name' => $user->getName(),
                    'role_id' => $user->getRole()->getId(),
                    'role_libelle' => $user->getRole()->getLibelle(),
                    'parent_id' => $user->getParent()
                );
            }

        $event->setData($data);


    }

}
