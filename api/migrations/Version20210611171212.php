<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210611171212 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE contract (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, child_id INT DEFAULT NULL, description LONGTEXT NOT NULL, INDEX IDX_E98F2859727ACA70 (parent_id), UNIQUE INDEX UNIQ_E98F2859DD62C21B (child_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rules (id INT AUTO_INCREMENT NOT NULL, contract_id INT DEFAULT NULL, libelle VARCHAR(255) NOT NULL, INDEX IDX_899A993C2576E0FD (contract_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tasks (id INT AUTO_INCREMENT NOT NULL, status_id INT DEFAULT NULL, users_id INT DEFAULT NULL, week_id INT DEFAULT NULL, libelle VARCHAR(255) NOT NULL, points INT NOT NULL, created_at DATETIME NOT NULL, date_end DATETIME NOT NULL, INDEX IDX_505865976BF700BD (status_id), INDEX IDX_5058659767B3B43D (users_id), INDEX IDX_50586597C86F3B2F (week_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE week (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, number_point INT NOT NULL, point_bonus INT NOT NULL, point_malus INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859727ACA70 FOREIGN KEY (parent_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859DD62C21B FOREIGN KEY (child_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE rules ADD CONSTRAINT FK_899A993C2576E0FD FOREIGN KEY (contract_id) REFERENCES contract (id)');
        $this->addSql('ALTER TABLE tasks ADD CONSTRAINT FK_505865976BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE tasks ADD CONSTRAINT FK_5058659767B3B43D FOREIGN KEY (users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE tasks ADD CONSTRAINT FK_50586597C86F3B2F FOREIGN KEY (week_id) REFERENCES week (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rules DROP FOREIGN KEY FK_899A993C2576E0FD');
        $this->addSql('ALTER TABLE tasks DROP FOREIGN KEY FK_505865976BF700BD');
        $this->addSql('ALTER TABLE tasks DROP FOREIGN KEY FK_50586597C86F3B2F');
        $this->addSql('DROP TABLE contract');
        $this->addSql('DROP TABLE rules');
        $this->addSql('DROP TABLE status');
        $this->addSql('DROP TABLE tasks');
        $this->addSql('DROP TABLE week');
    }
}
