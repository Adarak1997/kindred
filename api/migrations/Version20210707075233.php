<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210707075233 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE week ADD child_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE week ADD CONSTRAINT FK_5B5A69C0DD62C21B FOREIGN KEY (child_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_5B5A69C0DD62C21B ON week (child_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE week DROP FOREIGN KEY FK_5B5A69C0DD62C21B');
        $this->addSql('DROP INDEX IDX_5B5A69C0DD62C21B ON week');
        $this->addSql('ALTER TABLE week DROP child_id');
    }
}
