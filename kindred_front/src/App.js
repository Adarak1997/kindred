import React, {useState} from 'react';
import './App.css';
import { HashRouter, Route, Switch } from 'react-router-dom';
import ListContract from "./Pages/contracts/ListContract";
import AddContract from "./Pages/contracts/AddContract";
import Account from "./Pages/users/Account";
import Register from "./Pages/users/Register";
import Login from "./Pages/users/Login";
import {Profile} from "./Pages/users/Profile";
import { hasAuthenticated} from "./services/AuthApi";
import AuthenticatedRoute from "./Component/AuthenticatedRoute";
import Auth from "./contexts/Auth";
import MyChild from "./Pages/users/MyChild";
import AddChild from "./Pages/users/AddChild";
import ShowRulesContract from "./Pages/contracts/ShowRulesContract";
import AddTasks from "./Pages/tasks/AddTasks";
import ListTasks from "./Pages/tasks/ListTasks";
import Dashboard_child from "./Pages/users/dashboard_child";
import Historic from "./Pages/users/Historic";
import MyContract from "./Pages/contracts/MyContract";
import ValidateWeek from "./Pages/users/ValidateWeek";

function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(hasAuthenticated());
  return (
      <Auth.Provider value={{isAuthenticated, setIsAuthenticated}} >
    <HashRouter>

        <Switch>
          <AuthenticatedRoute exact path='/addcontract' component={AddContract} />
          <AuthenticatedRoute exact path='/listcontract' component={ListContract} />
          <AuthenticatedRoute exact path='/rules_contract' component={ShowRulesContract} />
          <AuthenticatedRoute path='/account' component={Account} />
          <AuthenticatedRoute path='/mychild' component={MyChild} />
          <AuthenticatedRoute path='/addchild' component={AddChild} />
          <AuthenticatedRoute path='/list_task' component={ListTasks} />
          <AuthenticatedRoute path='/addtask' component={AddTasks} />
          <AuthenticatedRoute path='/historic' component={Historic} />
          <AuthenticatedRoute path='/dashboard_child' component={Dashboard_child} />
          <AuthenticatedRoute path='/my_contract' component={MyContract} />
          <AuthenticatedRoute path='/validateWeek' component={ValidateWeek} />
          <Route exact path='/register' component={Register} />
          <Route exact path='/' component={Login} />
          <AuthenticatedRoute path='/profile/:id' component={Profile} />
        </Switch>
    </HashRouter>
        </Auth.Provider>
  );
}

export default App;
