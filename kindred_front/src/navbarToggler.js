import {Component} from "react";
import React from "react";
import ReactDOM from 'react-dom'
import App from "./App";
import menu from "./img/icon/menu.png";
import Navbar from "./Component/Navbar";
import logo from "./img/core-img/logo-white.png";


class TogglerNavbar extends Component{
    constructor(props) {
        super(props);
        this.state = {addClass: false}
    }


    toggle() {
        this.setState({addClass: !this.state.addClass});
    }

    render() {
        let boxToggle = ["open-menu"];
        if (this.state.addClass) {
            boxToggle.push('open-navbar-vertical');
        }
        return (
                <div className={boxToggle.join(' ')} onClick={this.toggle.bind(this)}>
                    <img src={menu} className="img-fluid"/>
                </div>


        );

    }
}

export default TogglerNavbar;
