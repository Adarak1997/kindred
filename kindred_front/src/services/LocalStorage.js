export function removeItem(removeItem) {
    window.localStorage.removeItem(removeItem);

}

export function getItem(item) {
    return window.localStorage.getItem(item);
}

export function addItem(localeStorageName, newItem) {
    window.localStorage.setItem(localeStorageName, newItem);
}
