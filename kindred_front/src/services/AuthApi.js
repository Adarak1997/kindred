import axios from 'axios';
import jwtDecode from "jwt-decode";
import { getItem, addItem, removeItem } from "./LocalStorage";

export function hasAuthenticated() {

    const token = getItem("user");
    const result = token ? tokenIsValid(token) : false;

    if(false === result) {
        removeItem("user");
    }
    return result;
}

export function login(credentials) {
    return axios
        .post('http://127.0.0.1:8000/api/login_check', credentials)
        .then((response) => {
            if (response.data.token) {
                addItem("user", JSON.stringify(response.data));
            }
            return response.data;
        });
};


export function logout() {
    removeItem("user");

}

export const getCurrentUser = () => {
    return JSON.parse(getItem("user"));
};

function tokenIsValid(token) {
    const { exp } = jwtDecode(token);

    if (exp * 1000 > new Date().getTime()) {
        return true;
    }

    return false;
}
