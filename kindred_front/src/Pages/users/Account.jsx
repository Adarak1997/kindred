import React, {Component} from 'react';
import {useState} from 'react';
import Profile from "./Profile";
import AddContract from "../contracts/AddContract";
import {getItem} from "../../services/LocalStorage";
import Navbar from "../../Component/Navbar";
import axios from "axios";
import {render} from "@testing-library/react";


class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            childs: []
        }
    }

    componentDidMount() {
        const user = JSON.parse(getItem("user")).data.id;

        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }

        axios.get("http://127.0.0.1:8000/api/users?parent=" + user, config).then(response => {
            this.setState({childs: response.data});
            if (!this.state.childs.length) {
                console.log("vide");
            } else {
                console.log("pas vide");
            }
        });
    }


    render() {
        return (
            <section className="margin-navbar">
                <Navbar/>
                <div className="container-fluid dashboard">
                    <div className="row">
                        <div className="col-lg-4 week-task">
                            <h1 className="mb-4">Les tâches de la semaine</h1>
                            <div className="list">
                                <div className="item valid">
                                    <h3 className="title">Sortir les poubelles</h3>
                                    <p className="name">Nom & Prénom</p>
                                    <span className="status">Réussis</span>
                                </div>
                                <div className="item valid">
                                    <h3 className="title">Débarasser la table</h3>
                                    <p className="name">Nom & Prénom</p>
                                    <span className="status">Réussis</span>
                                </div>
                                <div className="item waiting">
                                    <h3 className="title">Faire ses devoirs</h3>
                                    <p className="name">Nom & Prénom</p>
                                    <span className="status">En attente</span>
                                </div>
                                <div className="item invalid">
                                    <h3 className="title">Vider lave-vaisselle</h3>
                                    <p className="name">Nom & Prénom</p>
                                    <span className="status">Échec</span>
                                </div>
                                <div className="item invalid">
                                    <h3 className="title">Promener le chien</h3>
                                    <p className="name">Nom & Prénom</p>
                                    <span className="status">Échec</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        )
    }
}

export default Account;
