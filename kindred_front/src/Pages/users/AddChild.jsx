import React, {Component} from 'react';
import axios from "axios";
import {addItem, getItem, removeItem} from "../../services/LocalStorage";
import Navbar from "../../Component/Navbar";

export default class AddChild extends Component {


    componentDidMount() {
        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        return axios.get('http://127.0.0.1:8000/api/roles?libelle=enfant', config).then(
            (response) => {
                console.log(response.data);
                addItem("role_enfant", JSON.stringify(response.data));
                return response.data;

            }
        );
    }

    handleSubmit = e => {
        e.preventDefault();

        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        const getRoleEnfant = JSON.parse(getItem("role_enfant"));
        const role_enfant = getRoleEnfant[0].id;

        const parent_id = JSON.parse(getItem("user")).data.id;


        const data = {
            email: this.email,
            firstname: this.firstname,
            name: this.name,
            age: this.age,
            password: this.password,
            role: "http://127.0.0.1:8000/api/roles/" + role_enfant,
            parent: "http://127.0.0.1:8000/api/users/" + parent_id
        }


        axios.post('http://127.0.0.1:8000/api/users', data, config).then(
            res => {
                console.log(res);
                removeItem("role_enfant");
                this.props.history.push("/mychild");


            }
        ).catch(
            err => {
                console.log(err);
            }
        )
    };

    render() {
        return (
            <div>
                <Navbar/>
                <section className="page-form margin-navbar">
                    <div className="container">
                        <h1>Ajouter un enfant</h1>
                        <form className="form-profile" onSubmit={this.handleSubmit}>
                            <fieldset>
                                <div className="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" className="form-control"
                                           onChange={e => this.email = e.target.value}/>
                                </div>
                                <div className="form-group">
                                    <label>Prénom</label>
                                    <input type="text" name="firstname" className="form-control"
                                           onChange={e => this.firstname = e.target.value}/>
                                </div>
                                <div className="form-group">
                                    <label>Nom</label>
                                    <input type="text" name="name" className="form-control"
                                           onChange={e => this.name = e.target.value}/>
                                </div>
                                <div className="form-group">
                                    <label>Age</label>
                                    <input type="number" name="age" className="form-control"
                                           onChange={e => this.age = parseInt(e.target.value, 10)}/>
                                </div>
                                <div className="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" className="form-control"
                                           onChange={e => this.password = e.target.value}/>
                                </div>

                                <button type="submit" className="btn-theme">
                                    Ajouter l'enfant
                                </button>
                            </fieldset>
                        </form>
                    </div>

                </section>

            </div>
        );
    }
};
