import React, {Component} from 'react';
import Navbar from "../../Component/Navbar";
import {getItem} from "../../services/LocalStorage";
import axios from "axios";
import {NavLink} from "react-router-dom";
import Delete from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";


class MyChild extends Component {
    constructor(props) {
        super(props);
        this.state = {
            childs: []
        }
    }


    componentDidMount() {

        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }

        const getUserIDLogin = JSON.parse(getItem("user")).data.id;

        axios.get("http://127.0.0.1:8000/api/users?parent=" + getUserIDLogin, config).then(response => {
            this.setState({childs: response.data});
        });

    }

    /*const handle(){
            const id = this.state.childs.id;
            this.props.history("/profile/:id", { id });
    }*/

    render() {
        return (
            <section className="margin-navbar page-table">
                <Navbar/>
                <div className="container">
                    <h1 className="text-center color-theme">Liste de mes enfants</h1>
                    <div className="add-child">
                        <NavLink className="btn-theme" to="/addchild">Ajouter un enfant</NavLink>
                    </div>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">Nom</th>
                            <th scope="col">Prénom</th>
                            <th scope="col">Email</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                            {this.state.childs.map((item, key) => (
                                <tr key={key}>
                                    <td>{item.name}</td>
                                    <td>{item.firstname}</td>
                                    <td>{item.email}</td>
                                    <td>
                                        <Delete/>
                                        <EditIcon/>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </section>
        )
    }
}


export default MyChild;
