import React, {useContext, useEffect, useState} from 'react';
import Auth from "../../contexts/Auth";
import {login} from "../../services/AuthApi";
import {getItem} from "../../services/LocalStorage";
import {Link} from "react-router-dom";
import logo from "../../img/core-img/logo.png";



const Login = ({ history }) => {
    const { isAuthenticated, setIsAuthenticated } = useContext(Auth);
    const [user, setUser] = useState({
        username: "",
        password: ""

    })
    const handleChange = ({currentTarget}) => {
        const {name, value } = currentTarget;

        setUser({...user, [name]: value})

    }

    const handleSubmit = async event => {
        event.preventDefault();

        try {
            const response = await login(user);
            setIsAuthenticated(response);
            const role = JSON.parse(getItem("user"));
            if(role.data.role_libelle === "parent") {
                history.replace('/account');
            } else {
                history.replace('/dashboard_child');
            }


        }catch ({ response }) {
            console.log(response);
        }
    }

    useEffect(() => {
        if (isAuthenticated) {
            const role = JSON.parse(getItem("user"));
            if(role.data.role_libelle === "parent") {
                history.replace('/account');
            } else {
                history.replace('/dashboard_child');
            }
        }
    }, [history, isAuthenticated]
        );
    return (
        <section className="login bg-img">
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 mx-auto">
                        <div className="tab-content">
                            <div className="logo-container">
                                <img src={logo}/>
                                <h1>Kindred</h1>
                            </div>
                            <form className="form-profile" onSubmit={handleSubmit}>
                                <h3>Gérez le temps d'écran de  vos enfants avec facilité</h3>
                                <fieldset>
                                    <div className="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input type="text" name="username" className="form-control" id="email" onChange={handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="password">Mot de passe</label>
                                        <input type="password" name="password" className="form-control" id="password" onChange={handleChange}/>
                                    </div>

                                    <button type="submit" className="btn btn-theme">
                                        Connexion
                                    </button>
                                </fieldset>
                            </form>
                            <Link to="/register">Je n'ai pas de compte</Link>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    );
};
export default Login;
