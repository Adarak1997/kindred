import React, {Component} from 'react';
import axios from "axios";
import {Link, Redirect} from "react-router-dom";
import {addItem, getItem, removeItem} from "../../services/LocalStorage";
import logo from "../../img/core-img/logo.png";

export default class Register extends Component {


    componentDidMount() {
        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        return axios.get('http://127.0.0.1:8000/api/roles?libelle=parent', config).then(
            (response) => {
                addItem("role", JSON.stringify(response.data));
                return response.data;

            }
        );
    }

    handleSubmit = e => {
        e.preventDefault();

        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        const getRole = JSON.parse(getItem("role"));
        const role_parent = getRole[0].id;


        const data = {
            email: this.email,
            firstname: this.firstname,
            name: this.name,
            age: this.age,
            password: this.password,
            role: "http://127.0.0.1:8000/api/roles/" + role_parent
        }


        axios.post('http://127.0.0.1:8000/api/users', data, config).then(
            res => {
                console.log(res);
                removeItem("role");
                this.props.history.push("/");


            }
        ).catch(
            err => {
                console.log(err);
            }
        )
    };

    render() {
        return (

            <section className="login  bg-img">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 mx-auto">
                            <div className="tab-content">
                                <form className="form-profile h-auto" onSubmit={this.handleSubmit}>
                                    <fieldset>
                                        <h1>S'enregister</h1>
                                        <div className="form-group">
                                            <label>Email</label>
                                            <input type="email" name="email" className="form-control"
                                                   onChange={e => this.email = e.target.value}/>
                                        </div>
                                        <div className="form-group">
                                            <label>Prénom</label>
                                            <input type="text" name="firstname" className="form-control"
                                                   onChange={e => this.firstname = e.target.value}/>
                                        </div>
                                        <div className="form-group">
                                            <label>Nom</label>
                                            <input type="text" name="name" className="form-control"
                                                   onChange={e => this.name = e.target.value}/>
                                        </div>
                                        <div className="form-group">
                                            <label>Age</label>
                                            <input type="number" name="age" className="form-control"
                                                   onChange={e => this.age = parseInt(e.target.value, 10)}/>
                                        </div>
                                        <div className="form-group">
                                            <label>Password</label>
                                            <input type="password" name="password" className="form-control"
                                                   onChange={e => this.password = e.target.value}/>
                                        </div>

                                        <button type="submit" className="btn-theme">
                                            S'enregistrer
                                        </button>
                                    </fieldset>
                                </form>
                                <Link className="mt-5" to="/">Retour à la connexion</Link>

                            </div>
                        </div>
                    </div>
                </div>

            </section>

        );
    }
};

