import React, {Component} from 'react';
import axios from "axios";
import Navbar from "../../Component/Navbar";
import Carousel, { Dots } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import {getItem} from "../../services/LocalStorage";
import {Link} from "react-router-dom";


class ValidateWeek extends Component {
    constructor(props) {
        super(props);
        this.state = {
            weeks: [],
            childs: [],
            points: 0
        }
    }

    componentDidMount() {



        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }

        const userlogin = JSON.parse(getItem("user")).data.id;


        axios.get("http://127.0.0.1:8000/api/users?parent=" + userlogin, config).then(child => {
            this.setState({childs: child.data});
            for(let i = 0;i < this.state.childs.length; i++){
                axios.get("http://127.0.0.1:8000/api/weeks?child=" + this.state.childs[i].id + "&is_activated=" + true, config).then(response => {
                    if(response.data.length !== 0){
                        this.setState({weeks: this.state.weeks.concat(response.data)});
                        console.log(this.state.weeks);
                        console.log(this.state.weeks[0].tasks);

                    }

                });
            }
        });


    }

    validWeek(idWeek){
        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        const data_week = {
            isActivated: false
        }
        axios.put("http://127.0.0.1:8000/api/weeks/"+ idWeek ,data_week, config).then(response =>{
            console.log(response);

        })
    }

    validTask(idTask, idWeek){
        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        const data_task = {
            isValidated: true
        }
        axios.put("http://127.0.0.1:8000/api/tasks/"+ idTask ,data_task, config).then(response =>{
            console.log(response);

        });

        axios.get("http://127.0.0.1:8000/api/weeks/" + idWeek, config).then(response => {
            const data_week = {
                totalPoint: response.data.totalPoint + 5
            }
            axios.put("http://127.0.0.1:8000/api/weeks/" + idWeek,data_week, config).then(response => {
                this.setState({points: response.data.totalPoint});
            })
        })


    }


    render() {

        return(
            <section className="margin-navbar historic">
                <Navbar/>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-12 text-center">
                            <h1>Validation de la semaine</h1>
                        </div>
                    </div>
                    <div className="row carousel-bg">
                        <div className="col-lg-6 mx-auto">
                            <Carousel
                                plugins={[
                                    'infinite',
                                    'arrows',
                                    {
                                        resolve: slidesToShowPlugin,
                                        options: {
                                            numberOfSlides: 1
                                        }
                                    },
                                ]}
                                breakpoints={{
                                    991: {
                                        plugins: [
                                            {
                                                resolve: slidesToShowPlugin,
                                                options: {
                                                    numberOfSlides: 1,
                                                }
                                            },
                                        ]
                                    },
                                }}
                            >
                                {this.state.weeks.map((item, key)=> (
                                    <div className="item" key={key}>
                                        <div className="title">
                                            {item.length === 0 ? <h3></h3> :
                                                <h3>Semaine du {item.startWeek}</h3>
                                            }
                                            {item.length === 0 ? <h3></h3> :
                                                <h3>{item.child.firstname} {item.child.name}</h3>
                                            }

                                        </div>
                                        <ul className="task-list">
                                            {item.tasks.map((task, key) => (
                                                <li key={key}>{task.libelle}

                                                    {(task.isValidated) ?
                                                        <button className="btn-theme" disabled>Tâche validée</button> :

                                                        <button onClick={() => this.validTask(task.id, item.id)}
                                                                className="btn-theme">
                                                            Valider tâche
                                                        </button>
                                                    }</li>
                                            ))}

                                        </ul>
                                        <div className="footer">
                                            <p>Total de points : {this.state.points}</p>
                                            <Link className="mb-3" to="/account" onClick={() => this.validWeek(item.id)} className="btn-theme">
                                                Valider semaine
                                            </Link>
                                        </div>



                                    </div>
                                ))}
                            </Carousel>
                        </div>
                    </div>
                </div>
            </section>


        )
    }

}

export default ValidateWeek;
