import React, {Component} from "react";
import NavbarChild from "../../Component/NavbarChild";
import {getItem} from "../../services/LocalStorage";
import axios from "axios";
import Navbar from "../../Component/Navbar";
import {NavLink} from "react-router-dom";

class Dashboard_child extends Component {
    constructor(props) {
        super(props);
        this.state = {
            week: [],
            tasks: []
        }
    }

    componentDidMount() {
        const userlogin = JSON.parse(getItem("user")).data.id;
        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }

        axios.get("http://127.0.0.1:8000/api/weeks?is_activated=" + true, config).then(response => {
            this.setState({week: response.data});
            axios.get("http://127.0.0.1:8000/api/tasks?users=" + userlogin + "&week=" + this.state.week.id, config).then(
                response => {
                    this.setState({tasks: response.data});
                    console.log(this.state.tasks);
                }
            )
        });


    }

    render() {
        return(
            <section className="margin-navbar page-table">
                <NavbarChild/>
                <div className="container">
                    <h1 className="text-center color-theme">Listes de mes tâches</h1>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">description de la tâche</th>
                            <th scope="col">Points</th>
                            <th scope="col">Statut</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.tasks.map((item, key) => (
                            <tr key={key}>
                                <td>{item.libelle}</td>
                                <td>{item.points}</td>
                                <td>{item.status.libelle}</td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                    {this.state.week.length === 0 ?
                         <h1 className="total-point">total de point de la semaine : 0</h1> :
                        <h1 className="total-point">total de point de la semaine : {this.state.week[0].totalPoint}</h1>}

                </div>
            </section>
        )
    }
}

export default Dashboard_child;
