import Navbar from "../../Component/Navbar";
import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import {addItem, getItem} from "../../services/LocalStorage";
import axios from "axios";

class AddTasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            childs: [],
            tasks:[],
            index: -1
        }
    }

    componentDidMount() {
        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }

        const getUserIDLogin = JSON.parse(getItem("user")).data.id;

        axios.get("http://127.0.0.1:8000/api/users?parent=" + getUserIDLogin, config).then(
            response => {
                this.setState({childs: response.data});
            }
        )

        axios.get("http://127.0.0.1:8000/api/statuses?libelle=en cours", config).then(response => {
            addItem('status', JSON.stringify(response.data));
        });

    }

    addRule(){
        this.setState({tasks: [...this.state.tasks, "" ]})
        this.setState({index: this.state.index + 1})
    }

    handleRemove(index){
        this.state.tasks.splice(index,1)
        this.setState({tasks: this.state.tasks})
        this.setState({index: this.state.index - 1})
    }

    handleChange(e, index){
        this.state.tasks[index] = e.target.value
        this.setState({tasks: this.state.tasks})
    }


    handleSubmit = e => {

        e.preventDefault();

        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        const status = JSON.parse(getItem("status"));

        const user = JSON.parse(getItem("user"));

        const curr = new Date();
        const first = curr.getDate() - (curr.getDay()-1);
        const last = first + 6;
        const firstday = new Date(curr.setDate(first)).toLocaleDateString('FR', { day: '2-digit', month: '2-digit', year: 'numeric' });
        const lastday = new Date(curr.setDate(last)).toLocaleDateString('FR', { day: '2-digit', month: '2-digit', year: 'numeric' });


        const semaine = {
            startWeek: firstday,
            endWeek: lastday,
            totalPoint: 0,
            isActivated:true,
            child: "http://127.0.0.1:8000/api/users/" + this.childs

        }


            axios.get("http://127.0.0.1:8000/api/weeks?startWeek[after]=" + firstday + "&endWeek[before]=" + lastday + "&is_activated=" + true + "&child=" + this.childs, config).then(
                weekExist => {
                    if (weekExist.data.length === 1) {
                        for (let i = 0; i < this.state.index + 1; i++) {
                            const data_task = {
                                libelle: this.state.tasks[i],
                                points: 5,
                                week: "http://127.0.0.1:8000/api/weeks/" + weekExist.data[0].id,
                                status: "http://127.0.0.1:8000/api/statuses/" + status[0].id,
                                users: "http://127.0.0.1:8000/api/users/" + this.childs,
                                isValidated: false,
                            }
                            axios.post('http://127.0.0.1:8000/api/tasks', data_task, config).then(
                                res => {
                                    console.log(res);

                                }
                            ).catch(
                                err => {
                                    console.log(err);
                                }
                            );
                        }
                        this.props.history.push("/list_task");
                    } else {
                        axios.post('http://127.0.0.1:8000/api/weeks', semaine, config).then(
                            res => {
                                for (let i = 0; i < this.state.index + 1; i++) {
                                    const data_task = {
                                        libelle: this.state.tasks[i],
                                        points: 5,
                                        week: "http://127.0.0.1:8000/api/weeks/" + res.data.id,
                                        status: "http://127.0.0.1:8000/api/statuses/" + status[0].id,
                                        users: "http://127.0.0.1:8000/api/users/" + this.childs
                                    }
                                    axios.post('http://127.0.0.1:8000/api/tasks', data_task, config).then(
                                        res => {
                                            console.log(res);

                                        }
                                    ).catch(
                                        err => {
                                            console.log(err);
                                        }
                                    );
                                }
                                this.props.history.push("/list_task");
                            }
                        ).catch(
                            err => {
                                console.log(err);
                            }
                        );
                    }

                }
            )




    };


    render(){
        const curr = new Date();
        const first = curr.getDate() - (curr.getDay()-1);
        const last = first + 6;
        const firstday = new Date(curr.setDate(first)).toLocaleDateString('FR', { day: '2-digit', month: '2-digit', year: 'numeric' });
        const lastday = new Date(curr.setDate(last)).toLocaleDateString('FR', { day: '2-digit', month: '2-digit', year: 'numeric' });
        return(
            <div>
                <Navbar/>
                <section className="contract margin-navbar">
                    <div className="container">
                        <h1>Semaine du {firstday} au {lastday}</h1>
                        <form className="form-profile" onSubmit={this.handleSubmit}>
                            <fieldset>
                                {this.state.tasks.map((task , index) => (
                                    <div className="form-group row-rule" key={index}>
                                        <label>Tâche {index+1}</label>
                                        <input type="text" name="libelle" value={task} className="form-control" onChange={(e) =>this.handleChange(e, index)}/>
                                        <button type="button" className="btn-theme" onClick={()=>this.handleRemove(index)}>Supprimer</button>
                                    </div>
                                ))}
                                <button type="button" className="addRule" onClick={(e)=> this.addRule(e)}><strong>+</strong> Ajouter une tâche</button>
                                <div className="form-group">
                                    <label>Sélectionner l'enfant</label><br></br>
                                    <select className="form-select" onChange={e => this.childs = e.target.value}>
                                        <option value=""></option>
                                        {this.state.childs.map((item) =>
                                        <option value={item.id} key={item.id}>
                                            {item.firstname} {item.name}
                                        </option>
                                        )}
                                    </select>
                                </div>
                                <button type="submit" className="btn-theme">
                                    Enregistrer les tâches pour la semaine
                                </button>
                            </fieldset>
                        </form>
                    </div>

                </section>

            </div>
        )
    }
}

export default AddTasks;
