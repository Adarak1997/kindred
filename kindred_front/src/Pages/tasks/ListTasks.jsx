import Navbar from "../../Component/Navbar";
import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import {getItem} from "../../services/LocalStorage";
import axios from "axios";
import EditIcon from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";

class ListTasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            childs: []
        }
    }

    componentDidMount() {
        const getUserIDLogin = JSON.parse(getItem("user")).data.id;
        console.log(getUserIDLogin);
        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }


            axios.get("http://127.0.0.1:8000/api/users?parent=" + getUserIDLogin, config).then(responsechild => {
                this.setState({childs: responsechild.data});

                for(let i = 0; i <this.state.childs.length; i++){
                    axios.get("http://127.0.0.1:8000/api/tasks?child=" + this.state.childs[i].id, config).then(
                        responsetask => {
                            this.setState({tasks: responsetask.data});
                            console.log(this.state.tasks);
                        }
                    )
                }
            });





    }

    render(){
        return(
            <section className="margin-navbar page-table">
                <Navbar/>
                <div className="container">
                    <h1 className="text-center color-theme">Listes des tâches</h1>
                        <div className="add-child">
                            <NavLink to="/addtask"><button className="btn-theme">Créer les tâches de la semaine</button></NavLink>
                    </div>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">Prénom et nom de l'enfant</th>
                            <th scope="col">description de la tâche</th>
                            <th scope="col">Points</th>
                            <th scope="col">Statut</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.tasks.map((item, key) => (
                            <tr key={key}>
                                <td>{item.users.firstname} {item.users.name}</td>
                                <td>{item.libelle}</td>
                                <td>{item.points}</td>
                                <td>{item.status.libelle}</td>
                                <td>
                                    <EditIcon/> &ensp;
                                    <Delete/>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </section>
        )
    }
}

export default ListTasks;
