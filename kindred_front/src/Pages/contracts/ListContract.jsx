import React, {Component} from 'react';
import Navbar from "../../Component/Navbar";
import {addItem, getItem} from "../../services/LocalStorage";
import axios from "axios";
import {NavLink} from "react-router-dom";
import Delete from '@material-ui/icons/Delete';
import EditIcon from "@material-ui/icons/Edit";
import ListAltIcon from "@material-ui/icons/ListAlt";

class ListContract extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contracts: [],
            childs: []
        }
    }


    componentDidMount() {

        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }

        const getUserIDLogin = JSON.parse(getItem("user")).data.id;

        axios.all([
        axios.get("http://127.0.0.1:8000/api/contracts?parent=" + getUserIDLogin, config),

        axios.get("http://127.0.0.1:8000/api/users?parent=" + getUserIDLogin, config)
        ])
            .then(axios.spread((responsecontract, responseuser) => {
            this.setState({contracts: responsecontract.data});
            this.setState({childs: responseuser.data});

        }));



    }

    renderElement() {
        if (this.state.childs.length === this.state.contracts.length) {
            return <NavLink  to="/addcontract"><button className="btn-theme" disabled>Créer un contrat</button></NavLink>;
        } else {
            return <NavLink to="/addcontract"><button className="btn-theme">Créer un contrat</button></NavLink>;

        }
    }

    detailsRules(idcontract){
        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }

        axios.get("http://127.0.0.1:8000/api/contracts/" + idcontract, config).then(response => {
            addItem("contract", JSON.stringify(response.data));
            this.props.history.push('/rules_contract');

        });
    }

    render() {
        return (
            <section className="margin-navbar page-table">
                <Navbar/>
                <div className="container">
                    <h1 className="text-center color-theme">Liste de mes contrats</h1>
                    <div className="add-child">
                        {this.renderElement()}
                    </div>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">Prénom et nom de l'enfant</th>
                            <th scope="col">Libellé</th>
                            <th scope="col">Signature de l'enfant</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.contracts.map((item, key) => (
                            <tr key={key}>
                                <td>{item.child.firstname} {item.child.name}</td>
                                <td>{item.description}</td>
                                <td>{item.isSigned ? 'contrat signé' : "contrat non signé"}</td>
                                <td>
                                   <ListAltIcon onClick={() =>this.detailsRules(item.id)}>
                                   </ListAltIcon>
                                    &ensp;
                                    <EditIcon/> &ensp;
                                    <Delete/>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            </section>
        )
    }
}


export default ListContract;
