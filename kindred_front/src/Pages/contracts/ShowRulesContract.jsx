import React, {Component} from 'react';
import Navbar from "../../Component/Navbar";
import {addItem, getItem} from "../../services/LocalStorage";
import axios from "axios";
import {NavLink} from "react-router-dom";
import chevron from "../../img/icon/right-chevron.png"

class ShowRulesContract extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rules: []
        }
    }

    componentDidMount() {
        const contract = JSON.parse(getItem("contract"));
        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }

        axios.get("http://127.0.0.1:8000/api/rules?contract=" + contract.id, config).then(response => {
            this.setState({rules: response.data});

        });
    }

    render() {
        const contract = JSON.parse(getItem("contract"));
        return (
            <div>
                <Navbar/>
                <section className="contract margin-navbar">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-10 text-center mx-auto border-bottom pb-3">
                                <h1 className="mb-2">Détails du contrat</h1>
                                <h2>{contract.description}</h2>
                            </div>
                        </div>
                        <div className="col-lg-12 p-0 mx-auto">
                            <ul className="rules-list">
                                {this.state.rules.map((item, key) => (
                                    <li key={key} > <img src={chevron} className="mr-3" />{item.libelle}</li>
                                ))}
                            </ul>
                        </div>

                    </div>

                </section>

            </div>
        );

    }
}

export default ShowRulesContract;
