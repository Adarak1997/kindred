import React, {Component} from 'react';
import axios from "axios";
import {getItem} from "../../services/LocalStorage";
import Navbar from "../../Component/Navbar";


export default class AddContract extends Component {
    constructor(props) {
        super(props);
        this.state = {
            childs: [],
            rules:[],
            index: -1,
            contracts:[],
            childcontract: [],
            childnotcontract: []
        }
    }

    componentDidMount() {

        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }

        const getUserIDLogin = JSON.parse(getItem("user")).data.id;

        axios.all([
        axios.get("http://127.0.0.1:8000/api/users?parent=" + getUserIDLogin, config),
            axios.get("http://127.0.0.1:8000/api/contracts?parent=" + getUserIDLogin, config)
            ]).then(axios.spread((responsechild, responsecontract) => {
            this.setState({childs: responsechild.data});
            this.setState({contracts: responsecontract.data});
            if(this.state.contracts.length !== 0){
                for(let i = 0; i < this.state.childs.length; i++){
                    for(let j = 0 ; j< this.state.childs.length; j ++) {
                        if (typeof this.state.contracts[i] === 'undefined') {
                        } else if (this.state.contracts[i].child.id === this.state.childs[j].id) {
                            this.setState({childcontract: this.state.childs[j]});

                        } else {
                            this.setState({childnotcontract: this.state.childs[j]});
                        }
                    }

                }
            }else{
                this.setState({childnotcontract: this.state.childs});
            }

        }));



    }

    addRule(){
        this.setState({rules: [...this.state.rules, "" ]})
        this.setState({index: this.state.index + 1})
        console.log(this.state.index);
    }

    handleChange(e, index){
        this.state.rules[index] = e.target.value
        this.setState({rules: this.state.rules})
        console.log(this.state.rules);


    }

    handleRemove(index){
        this.state.rules.splice(index,1)
        console.log(this.state.rules);
        this.setState({rules: this.state.rules})
        this.setState({index: this.state.index - 1})
        console.log(this.state.index);
    }


    handleSubmit = e => {
        e.preventDefault();

        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        const user = JSON.parse(getItem("user"));
        console.log(user.data.id);


        const data = {
            description: this.description,
            isSigned: false,
            parent: "http://127.0.0.1:8000/api/users/" + user.data.id,
            child: "http://127.0.0.1:8000/api/users/" + this.childnotcontract

        }


        axios.post('http://127.0.0.1:8000/api/contracts', data, config).then(
            res => {
                for(let i = 0; i < this.state.index+1; i++) {
                const data_rule = {
                    libelle: this.state.rules[i],
                    contract: "http://127.0.0.1:8000/api/contracts/" + res.data.id
                }
                    axios.post('http://127.0.0.1:8000/api/rules', data_rule, config).then(
                        res => {
                            console.log(res);

                        }
                    ).catch(
                        err => {
                            console.log(err);
                        }
                    );
                }
                this.props.history.push("/listcontract");
            }
        ).catch(
            err => {
                console.log(err);
                console.log(data);
            }
        );

    };

renderElement(){
    if(this.state.childnotcontract.length > 1) {
        return this.state.childnotcontract.map((item) =>
            <option value={item.id} key={item.id}>
                {item.firstname} {item.name}
            </option>
        )
    }else{
        return <option value={this.state.childnotcontract.id}>
            {this.state.childnotcontract.firstname} {this.state.childnotcontract.name}
        </option>;
    }
}

render(){


        return (

    <div>
        <Navbar />
        <section className="contract margin-navbar">
            <div className="container">
                <h1>Ajouter un contrat</h1>
                <form className="form-profile" onSubmit={this.handleSubmit}>
                    <fieldset>
                        <div className="form-group">
                            <label>Libelle du contrat</label>
                            <input type="text" name="description" className="form-control"  onChange={e => this.description = e.target.value}/>
                        </div>
                        {this.state.rules.map((rule , index) => (
                            <div className="form-group row-rule" key={index}>
                                <label>Règle {index+1}</label>
                                <input type="text" name="libelle" value={rule} className="form-control" onChange={(e) =>this.handleChange(e, index)}/>


                                <button type="button" className="btn-theme" onClick={()=>this.handleRemove(index)}>Supprimer</button>
                            </div>
                        ))}
                        <button type="button" className="addRule" onClick={(e)=> this.addRule(e)}><strong>+</strong> Ajouter une règle</button>
                        <div className="form-group">
                            <label>Sélectionner l'enfant</label><br></br>
                            <select className="form-select" onChange={e => this.childnotcontract = e.target.value}>
                                <option value=""></option>
                                {this.renderElement()}
                            </select>
                        </div>
                        <button type="submit" className="btn-theme">
                            Créer contrat
                        </button>
                    </fieldset>
                </form>
            </div>

        </section>

    </div>);

        }
};


