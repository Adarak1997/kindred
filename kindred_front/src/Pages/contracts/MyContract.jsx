import React, {Component} from 'react';
import NavbarChild from "../../Component/NavbarChild";
import {addItem, getItem} from "../../services/LocalStorage";
import axios from "axios";
import {Link, NavLink} from "react-router-dom";
import { useHistory } from 'react-router-dom';

class MyContract extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rules: []
        }
    }

    componentDidMount() {

        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }

        const contract = JSON.parse(getItem("contract"));

            axios.get("http://127.0.0.1:8000/api/rules?contract=" + contract.id, config).then(response =>{
                this.setState({rules: response.data});

            });

    }

    validContract(){
        const contract = JSON.parse(getItem("contract"));
        const config = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
        const data_contract = {
            isSigned: true
        }
        axios.put("http://127.0.0.1:8000/api/contracts/"+ contract[0].id ,data_contract, config).then(response =>{

        })
    }

    render() {
        const contract = JSON.parse(getItem("contract"));
        return (
            <div>
                <NavbarChild/>

                <section className="contract margin-navbar">
                    <div className="container">
                        <h1>Détails du contrat {contract.description}</h1>
                        <fieldset>
                            <div className="form-group">
                                <h3>{contract[0].description}</h3>
                            </div>
                            {this.state.rules.map((item, key) => (
                                <div key={key}>
                                    <h2>{item.libelle}</h2>
                                </div>
                            ))}
                        </fieldset>
                        <Link to="/dashboard_child" onClick={this.validContract} className="btn-theme">
                            Valider les règles du contrat
                        </Link>
                    </div>

                </section>


            </div>
        );

    }
}

export default MyContract;
