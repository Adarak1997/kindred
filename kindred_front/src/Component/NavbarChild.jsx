import React, {useContext} from 'react';
import {NavLink} from "react-router-dom";
import Auth from "../contexts/Auth";
import {logout} from "../services/AuthApi";
import logo from "../img/core-img/logo-white.png";
import close from "../img/icon/close.png";
import menu from "../img/icon/menu.png";
import {addItem, getItem} from "../services/LocalStorage";
import axios from "axios";


const NavbarChild = () => {
    const { isAuthenticated, setIsAuthenticated } = useContext(Auth);

    const handleLogout = () => {
        logout();
        setIsAuthenticated(false);
    }
    const config = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }
    const userlogin = JSON.parse(getItem("user")).data.id;

    axios.get("http://127.0.0.1:8000/api/contracts?child=" + userlogin, config).then(response => {
        addItem("contract", JSON.stringify(response.data));


    });

    function openNavbarVertical() {
        var element = document.getElementsByClassName(".navbar-vetical");
        element.classList.remove("close-navbar-vertical");
        element.classList.add("open-navbar-vertical")
    }

    function closeNavbarVertical() {
        var element = document.getElementsByClassName(".navbar-vetical");
        element.classList.remove("open-navbar-vertical")
        element.classList.add("close-navbar-vertical");
    }

    return (
        // <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        //     <div className="container-fluid">
        //         <span className="navbar-brand">Kindred</span>
        //         <div className="collapse navbar-collapse" id="navbarColor02">
        //             <ul className="navbar-nav me-auto">
        //                 <li className="nav-item">
        //                     <NavLink className="nav-link active" to="/mychild">
        //                         Mes enfants
        //                     </NavLink>
        //                 </li>
        //                 <li>
        //                     <NavLink className="nav-link active" to="/addchild">
        //                         Ajouter enfant
        //                     </NavLink>
        //                 </li>
        //                 </ul>
        //             <ul className="navbar-nav ml-auto">
        //                     { (!isAuthenticated && (
        //                     <>
        //                     <li className="nav-item">
        //                         <NavLink className="nav-link active" to="/login">
        //                             Connexion
        //                         </NavLink>
        //                     </li>
        //                     <li className="nav-item">
        //                         <NavLink className="nav-link active" to="/register">
        //                             S'enregister
        //                         </NavLink>
        //                     </li>
        //                     </>
        //                     )) || (
        //                         <>
        //                         <li className="nav-item">
        //                             <NavLink className="nav-link active" to="/account">
        //                                 Mon compte
        //                             </NavLink>
        //                         </li>
        //                         <li className="nav-item">
        //                             <button className="btn btn-danger" onClick={handleLogout}>Déconnexion</button>
        //                         </li>
        //                         </>
        //                     )}
        //             </ul>
        //         </div>
        //     </div>
        // </nav>
        <div>
            <div className="margin-navbar"></div>
            <nav className="navbar-horizontal">
                <div className="open-menu" onClick={openNavbarVertical}>
                    <img src={menu} className="img-fluid"/>
                </div>
                <div className="logo-container">
                    <img src={logo} className="img-fluid"/>
                </div>
            </nav>
            <nav className="navbar-vertical">
                <div className="cross" onClick={closeNavbarVertical}>
                    <img src={close} className="img-fluid"/>
                </div>
                <div className="logo-container">
                    <img src={logo} className="img-fluid"/>
                </div>
                <div className="navigation">
                    <div className="nav-item">
                        <NavLink to="/dashboard_child" className="lien-absolute"></NavLink>
                        <div className="icon"></div>
                        <div className="text">
                            <p>Mes tâches</p>
                        </div>
                    </div>
                    <div className="nav-item">
                        <NavLink to="/my_contract" className="lien-absolute"></NavLink>
                        <div className="icon"></div>
                        <div className="text">
                            <p>Mon contrat</p>
                        </div>
                    </div>
                </div>
                <div className="nav-item disconect">
                    <a onClick={handleLogout} className="lien-absolute"></a>
                    <div className="icon"></div>
                    <div className="text">
                        <p>Déconnexion</p>
                    </div>
                </div>
            </nav>
        </div>

    );
};

export default NavbarChild;
