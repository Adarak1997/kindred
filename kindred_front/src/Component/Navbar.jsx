import React, {useContext} from 'react';
import {NavLink} from "react-router-dom";
import Auth from "../contexts/Auth";
import {logout} from "../services/AuthApi";
import logo from "../img/core-img/logo-white.png";
import menu from "../img/icon/menu.png";
import navbarToggler from "../navbarToggler";
import TogglerNavbar from "../navbarToggler";
import CrossNavbar from "../crossNavbar";

const Navbar = () => {
    const { isAuthenticated, setIsAuthenticated } = useContext(Auth);

    const handleLogout = () => {
        logout();
        setIsAuthenticated(false);
    }


    // function openNavbarVertical() {
    //     var element = document.getElementsByClassName(".navbar-vetical");
    //     element.classList.remove("close-navbar-vertical");
    //     element.classList.add("open-navbar-vertical")
    // }
    //
    // function closeNavbarVertical() {
    //     var element = document.getElementsByClassName(".navbar-vetical");
    //     element.classList.remove("open-navbar-vertical")
    //     element.classList.add("close-navbar-vertical");
    // }

    return (
        <div>
            <TogglerNavbar></TogglerNavbar>

            <div className="margin-navbar"></div>
            <nav className="navbar-horizontal">
                {/*<div className="open-menu">*/}
                {/*    <img src={menu} className="img-fluid"/>*/}
                {/*</div>*/}
                <div className="logo-container">
                    <img src={logo} className="img-fluid"/>
                </div>
            </nav>
            <nav className="navbar-vertical" id="nav">
                {/*<div className="cross" onClick={closeNavbarVertical}>*/}
                {/*    <img src={close} className="img-fluid"/>*/}
                {/*</div>*/}
                <div className="logo-container">
                    <img src={logo} className="img-fluid"/>
                </div>
                <div className="navigation">
                    <div className="nav-item">
                        <NavLink to="/account" className="lien-absolute"></NavLink>
                        <div className="icon"></div>
                        <div className="text">
                            <p>Tableau de bord</p>
                        </div>
                    </div>
                    <div className="nav-item">
                        <NavLink to="/mychild" className="lien-absolute"></NavLink>
                        <div className="icon"></div>
                        <div className="text">
                            <p>Mes enfants</p>
                        </div>
                    </div>
                    <div className="nav-item">
                        <NavLink to="/list_task" className="lien-absolute"></NavLink>
                        <div className="icon"></div>
                        <div className="text">
                            <p>Les tâches</p>
                        </div>
                    </div>
                    <div className="nav-item">
                        <NavLink to="/listcontract" className="lien-absolute"></NavLink>
                        <div className="icon"></div>
                        <div className="text">
                            <p>Contrat</p>
                        </div>
                    </div>
                    <div className="nav-item">
                        <NavLink to="/historic" className="lien-absolute"></NavLink>
                        <div className="icon"></div>
                        <div className="text">
                            <p>Historique</p>
                        </div>
                    </div>
                    <div className="nav-item">
                        <NavLink to="/validateWeek" className="lien-absolute"></NavLink>
                        <div className="icon"></div>
                        <div className="text">
                            <p>Valider semaine</p>
                        </div>
                    </div>
                </div>

                <div id="root">
                </div>
                <div className="nav-item disconect">
                    <a onClick={handleLogout} className="lien-absolute"></a>
                    <div className="icon"></div>
                    <div className="text">
                        <p>Déconnexion</p>
                    </div>
                </div>
            </nav>
        </div>

    );
};

export default Navbar;


