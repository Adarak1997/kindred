import {Component} from "react";
import React from "react";
import ReactDOM from 'react-dom'
import App from "./App";
import menu from "./img/icon/menu.png";
import Navbar from "./Component/Navbar";
import logo from "./img/core-img/logo-white.png";
import cross from "./img/icon/close.png";


class CrossNavbar extends Component{
    constructor(props) {
        super(props);
        this.state = {addClass: false}
    }


    toggle() {
        this.setState({addClass: !this.state.addClass});
    }

    render() {
        let boxToggle = ["cross"];
        if (this.state.addClass) {
            boxToggle.push('close-navbar-vertical');
        }
        return (
                <div className={boxToggle.join(' ')} onClick={this.toggle.bind(this)}>
                    <img src={cross} className="img-fluid"/>
                </div>


        );

    }
}

export default CrossNavbar;
